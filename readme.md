#    Creating arcs in ObjectiveC   

* [JSFiddle](http://jsfiddle.net/vreemt/4LCMC/12/)
* See also <http://jsfiddle.net/alexdickson/89mQx/>

## Resources
* [mapsbox arcjs](https://www.mapbox.com/mapbox.js/example/v1.0.0/arcjs/) 
* [mozilla developer network HTML5 canvas](https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Canvas_tutorial/Drawing_shapes) 
* [trigonometry](https://www.math.hmc.edu/calculus/tutorials/reviewtriglogexp/) 
* [Ray Wenderlich - arcs in ObjectiveC](http://www.raywenderlich.com/33193/core-graphics-tutorial-arcs-and-paths) 
* [arc between 2 points](http://stackoverflow.com/questions/5439462/html-canvas-draw-arc-between-two-points) 
* [html5 canvas arc](http://www.html5canvastutorials.com/tutorials/html5-canvas-arcs/) 
* [draw dot on html5 canvas](http://stackoverflow.com/questions/7812514/drawing-a-dot-on-html5-canvas)
* [Javascript Math](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math)
* [Finding the coordinates on the edge of a circle](http://stackoverflow.com/questions/2093405/finding-the-coordinates-on-the-edge-of-a-circle)

## Steps
* [acos](http://www.opengroup.org/onlinepubs/009695399/functions/acos.html)
* [pow](http://www.opengroup.org/onlinepubs/009695399/functions/pow.html)


**Copied from <http://www.raywenderlich.com/33193/core-graphics-tutorial-arcs-and-paths>**   

Open up _Common.h_ and add the following to the bottom of the file:

	static inline double radians (double degrees) { return degrees * M_PI/180; }
    
	CGMutablePathRef createArcPathFromBottomOfRect(CGRect rect, CGFloat arcHeight);

The first thing you add is a helper function to convert degrees to radians.


You only need the actual arc from the `CGMutablePathRef`

_Common.m_
    
	CGMutablePathRef createArcPathFromBottomOfRect(CGRect rect, CGFloat arcHeight) 
	{ 
		CGRect arcRect = CGRectMake(rect.origin.x, rect.origin.y + rect.size.height - arcHeight, rect.size.width, arcHeight);
 
		CGFloat arcRadius = (arcRect.size.height/2) + (pow (arcRect.size.width, 2) / (8*arcRect.size.height));
		CGPoint arcCenter = CGPointMake(arcRect.origin.x + arcRect.size.width/2, arcRect.origin.y + arcRadius);
 
		CGFloat angle = acos (arcRect.size.width / (2*arcRadius));
		CGFloat startAngle = radians(180) + angle;
		CGFloat endAngle = radians(360) - angle;
 
		CGMutablePathRef path = CGPathCreateMutable();
		CGPathAddArc(path, NULL, arcCenter.x, arcCenter.y, arcRadius, startAngle, endAngle, 0);
 
		return path;    
	}

## Implementation

Your rectangle would be the surrounding box for the sunrise/sunset  

* Arc width/height would be the same as rectangle width/height
* angle will be `acos(1)` if arc width is the same as diameter  


    Sunset = endAngle = endTime;
    Sunrise = startAngle = startTime;
    Current time in same format (Timestamp) as start/end time
    Sun visible: currentTime >= startTime && currentTime <= endTime


Calculations where ‘now’ or the `currentTime` would be only need to happen if this point is visible.

	totalAngle = endAngle – startAngle;
	totalTime = endTime – startTime;

Point along the path would be based on `angle = (totalAngle / totalTime) / currentTime;`
//calculate angle based on value using ranges

	CGRect arcRect = CGRectMake(rect.origin.x, rect.origin.y + rect.size.height - arcHeight, rect.size.width, arcHeight);
 
	CGFloat arcRadius = (arcRect.size.height/2) + (pow (arcRect.size.width, 2) / (8*arcRect.size.height));
	CGPoint arcCenter = CGPointMake(arcRect.origin.x + arcRect.size.width/2, arcRect.origin.y + arcRadius);
 
 	CGFloat angle = acos (arcRect.size.width / (2*arcRadius));
	CGFloat startAngle = radians(180) + angle;
	CGFloat endAngle = radians(360) - angle;


Ranges - use angle/value quotient to calcute angle for point
Check:

	if (currentTime >= startTime && currentTime <= endTime) {
		//draw point
	}

or 
	
	if (currentTime < startTime || currentTime > endTime) {
		//don't draw point
	}


You can probably get the variables from `path`

	//arcCenter.x, arcCenter.y, arcRadius 
	//myAngle = currentHour/12 * M_PI (angle in rad ranges from 0..2 * pi)

	CGFloat myAngle = (1+(currentHour/24)) * M_PI;

	CGFloat? pointX = arcCenter.x + arcRadius * Math.Cos(myAngle); 
	CGFloat? pointY = arcCenter.y + arcRadius * Math.Sin(myAngle); 
	CGPoint? myPoint = CGPointMake(pointX, pointY);

Points x,y
    
    //x,y centre of arc; x1,y1 point on arc
    x = arcRect.origin.x + arcRect.size.width/2;
    y = arcRect.origin.y + arcRadius;

    x1 = x + arcRadius * Math.Cos(angle * (M_PI / 180));
    y1 = y + arcRadius * Math.Sin(angle * (M_PI / 180));
    //where all variables are doubles and angle is in degrees, measured anticlockwise from the x-axis

    //from point edge left (180 deg, 1*pi in radians), where x,y is centre

    nowAngle = (1+(currentHour/24)) * M_PI;
    x2 = x + arcRadius * Math.Cos(nowAngle);
    y2 = y + arcRadius * Math.Sin(nowAngle);

Or in different words - for a circle with origin (j, k), radius r, and angle t in radians:

    x(t) = r * cos(t) + j       
    y(t) = r * sin(t) + k


## More Math

* All circles start on the right, centered around 0,0 in the middle
* Bottom of circle is 0.5 * Pi, mid left 1 * Pi, top mid 1.5 * Pi, mid right 2 * Pi
* Default - circles are drawn clockwise
* Could probably do with a time-to-radians function!

## Javascript

* HTML canvas starts 0,0 top left
* Functions in JS: Math.PI, Math.acos(), Math.pow(base, exponent), Math.sin(), Math.tan(), Math.sqrt(x) 
* Note that the trigonometric functions (sin, cos, tan, asin, acos, atan, atan2) expect or return angles in radians. To convert radians to degrees, divide by (Math.PI / 180), and multiply by this to convert the other way.

